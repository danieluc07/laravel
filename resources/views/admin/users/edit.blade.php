@extends('admin/template/main')

@section('title','Crear Usuario')

@section('content')

	{!! Form::open(['route'=>['admin.users.update',$user],'method'=>'PUT']) !!}
	
	<div class="form-group">
		{!! Form::label('name','Nombre') !!}
		{!! Form::text('name',$user->name,['class'=>'form-control','placeholder'=>'Nombre Completo','required']) !!}
	</div>
    
    <div class="form-group">
		{!! Form::label('email','Correo') !!}
		{!! Form::email('email',$user->email,['class'=>'form-control','placeholder'=>'yo@bd.com','required']) !!}
	</div>


	<div class="form-group">
		{!! Form::label('type','Tipo') !!}
		{!! Form::select('type',['member'=>'Usuario','admin'=>'Administrador'],$user->type,['class'=>'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Guardar Cambios',['class'=>'btn btn-primary']) !!}
		
	</div>

	{!! Form::close() !!}


@endsection