@extends('admin/template/main')

@section('title','Crear Usuario')

@section('section')
	<h3>Crear Usuario</h3>
@endsection

@section('content')

	{!! Form::open(['route'=>'admin.users.store','method'=>'POST']) !!}
	
	<div class="form-group">
		{!! Form::label('name','Nombre') !!}
		{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Nombre Completo','required']) !!}
	</div>
    
    <div class="form-group">
		{!! Form::label('email','Correo') !!}
		{!! Form::email('email',null,['class'=>'form-control','placeholder'=>'yo@bd.com','required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('password','Contraseña') !!}
		{!! Form::password('password',['class'=>'form-control','placeholder'=>'*********','required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('type','Tipo') !!}
		{!! Form::select('type',['member'=>'Usuario','admin'=>'Administrador'],null,['class'=>'form-control']) !!}
	</div>

	<div class="form-group">
		{!! Form::submit('Registrar',['class'=>'btn btn-primary']) !!}
		
	</div>

	{!! Form::close() !!}


@endsection


