@extends('admin.template.main')

@section('title',' Agregar Articulo')

@section('section')
	<h3>Editar Articulo</h3>
@endsection

@section('content')
	{!! Form::open(['route'=>['admin.articles.update',$article,$article->id],'method'=>'PUT','files'=>true]) !!}

	<div class="form-group">
		{!! Form::label('title','Titulo')!!}
		{!! Form::text('title',$article->title,['class'=>'form-control','placeholder'=>'Titulo del articulo','required']) !!}
	</div>

	<div class="form-group">
		{!! Form::label('category_id','Categoria: ')!!}
		{!! Form::select('category_id',$categories,$article->category_id,['class'=>'form-control select-tag','required']) !!}
	</div>

	
	<div class="form-group">
		{!! Form::label('content','Contenido') !!}
		{!! Form::textarea('content',$article->content,['class'=>'form-control textarea-content']) !!}
	</div>
	
	<div class="form-group">
		{!! Form::label('tags','Tags') !!}
		{!! Form::select('tags[]',$tags,$article->tags->lists('id')->ToArray(),['class'=>'form-control select-tag','multiple','required']) !!}
	</div>

	<!--<div class="form-group">
		{!! Form::label('image','Imagen') !!}
		{!! Form::file('image') !!}
	</div>-->
	<div class="form-group">
		{!!Form::submit('Editar Articulo',['class'=>'btn btn-primary']) !!}
	</div>


	{!! Form::close() !!}

@endsection

@section('js')
	<script>
	$('.select-tag').chosen({
		placeholder_text_multiple:'Seleccione una opción',
		max_selected_options:3,
		search_contains:true,
		no_result_text:'No hay categorias'
	});

	$('.select-category').chosen({
		placeholder_text_single:'Selecciona una categoria'
	});

	$('.textarea-content').trumbowyg();

</script>
@endsection