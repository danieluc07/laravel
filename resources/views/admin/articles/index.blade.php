@extends('admin.template.main')
@section('title','Listado de articulos')

@section('section')
	<h3>Listado de articulos</h3>
@endsection

@section('content')

	

	<a href="{{route('admin.articles.create')}}"><button class="btn btn-info">Crear Articulo</button></a>

	<!-- BUSCADOR -->

	{!! Form::open(['route'=>'admin.articles.index','method'=>'GET','class'=>'navbar-form pull-right']) !!}

		<div class="input-group">
			{!! Form::text('title',null,['class'=>'form-control','placeholder'=>'Buscar Articulo','aria-describedby'=>'search']) !!}
			<span class="input-group-addon" id="search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
		</div>
	{!! Form::close() !!}

	<!-- BUSCADOR -->

	<table class="table table-striped">
		<thead>
			<th>ID</th>
			<th>Titulo</th>
			<th>Categorias</th>
			<th>Usuario</th>
			<th>Acción</th>
		</thead>
		<tbody>
			@foreach($articles as $article)
			<tr>
				<td>{{ $article->id }}</td>
				<td>{{ $article->title }}</td>
				<td>{{ $article->category->name }}</td>
				<td>{{ $article->user->name }}</td>
				<td>
					<a href="{{route('admin.articles.edit',$article->id)}}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="True"></span></a>
					<a href="{{route('admin.articles.destroy',$article->id)}}" class="btn btn-danger" onclick="return confirm('¿Deseas Eliminar el usuario?')"><span class="glyphicon glyphicon-remove-circle" aria-hidden="True"></span></a>
				</td>
			</tr>

			@endforeach
		</tbody>
	</table>
	<div class="text-center">
		{!! $articles->render() !!}
	</div>
@endsection()