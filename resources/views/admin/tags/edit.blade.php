@extends('admin.template.main')

@section('title','Editar Etiqueta')

@section('content')
	{!! Form::open(['route'=>['admin.tags.update', $tags],'method'=>'PUT']) !!}
	<div class="form-group">
		{!! Form::label('name','Nombre')!!}
		{!! Form::text('name',$tags->name,['class'=>'form-control','placeholder'=>'Nombre de la Categoria','required']) !!}
	</div>
	<div class="form-group">
		{!!Form::submit('Editar',['class'=>'btn btn-primary']) !!}
	</div>
		
	{!! Form::close() !!}

@endsection