@extends('admin.template.main')
@section('title','Listado de Categorias')

@section('section')
	<h3>Listado de etiquetas</h3>
@endsection

@section('content')
	<a href="{{route('admin.tags.create')}}"><button class="btn btn-info">Registrar Etiqueta</button></a>

	<!-- BUSCADOR -->

	{!! Form::open(['route'=>'admin.tags.index','method'=>'GET','class'=>'navbar-form pull-right']) !!}

		<div class="input-group">
			{!! Form::text('name',null,['class'=>'form-control','placeholder'=>'Busta etiqueta','aria-describedby'=>'search']) !!}
			<span class="input-group-addon" id="search"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></span>
		</div>

	{!! Form::close() !!}

	<!-- BUSCADOR -->

	<table class="table table-striped table-bordered">
	<thead>
		<th>ID</th>
		<th>Nombre</th>
		<th>Acción</th>
	</thead>
	<tbody>
		@foreach($tags as $tag)
		<tr>
			<td>{{ $tag->id }}</td>
			<td>{{ $tag->name }}</td>
			<td>
					<a href="{{route('admin.tags.edit',$tag->id)}}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="True"></span></a>
					<a href="{{route('admin.tags.destroy',$tag->id)}}" class="btn btn-danger" onclick="return confirm('¿Deseas Eliminar la etiqueta?')"><span class="glyphicon glyphicon-remove-circle" aria-hidden="True"></span></a>
			</td>
		</tr>
		@endforeach
	</tbody>
	</table>
	<div class="text-center">
		{!! $tags->render() !!}
	</div>
@endsection()