<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->

    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{route('/')}}">Blog</a>
    </div>


    

     <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      @if(Auth::user())
      
      @if(Auth::user()->isAdmin())
      <ul class="nav navbar-nav">  
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Usuarios<span class="caret"></span></a>
          <ul class="dropdown-menu">
             <li class="active"><a href="{{route('admin.users.index')}}">Lista de usuarios<span class="sr-only">(current)
            <li role="separator" class="divider"></li>
            <li><a href="{{route('admin.users.create')}}">Crear usuario</a></li>
          </ul>
        </li>
      </ul>
      @endif
    
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Categorias<span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="{{route('admin.categories.index')}}">Lista de categorias</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{route('admin.categories.create')}}">Crear categorias</a></li>
          </ul>
        </li>
      </ul>

    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Etiquetas<span class="caret"></span></a>
          <ul class="dropdown-menu">
  
            <li><a href="{{route('admin.tags.index')}}">Lista de etiquetas</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{route('admin.tags.create')}}">Crear etiquetas</a></li>
          </ul>
        </li>
      </ul>

      <ul class="nav navbar-nav">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Articulos<span class="caret"></span></a>
          <ul class="dropdown-menu">
  
             <li><a href="{{route('admin.articles.index')}}">Lista de articulos</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="{{route('admin.articles.create')}}">Crear Articulo</a></li>
          </ul>
        </li>
        <li><a href="{{ route('admin.images.index')}}">Imagenes</a></li>
      </ul>
      
  @endif



<!-- Authentication Links -->
      <ul class="nav navbar-nav navbar-right">
        @if (Auth::guest())
          <li><a href="{{ url('/login') }}">Entrar</a></li>
          <li><a href="{{ url('/register') }}">Registrarse</a></li>
        @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Salir</a></li>
                            </ul>
                        </li>
                    @endif

    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>


