<!DOCTYPE html>
<html lang="es">
    <head>
        <meta charset="utf-8">
        <meta content="width=device-width, initial-scale=1" name="viewport">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link href="{{asset('plugins/bootstrap/css/bootstrap.css')}}" rel="stylesheet">
        <link href="{{asset('css/pp.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('plugins/chosen/chosen.css')}}" rel="stylesheet" type="text/css">
        <link href="{{asset('plugins/trumbowyg/ui/trumbowyg.min.css')}}" rel="stylesheet" type="text/css">
        
        <title>@yield('title','Default')</title>
    </head>
    <body>
        <script src="{{asset('plugins/jquery.min.js')}}">
        </script>
        <script src="{{asset('plugins/bootstrap/js/bootstrap.js')}}">
        </script>
        <script src="{{asset('plugins/chosen/chosen.jquery.min.js')}}">
        </script>
        <script src="{{asset('plugins/trumbowyg/trumbowyg.min.js')}}">
        </script>
        <div class="container">
            @include('admin/template/partials/nav')
		@yield('section')
            <div class="alert">
                @include('flash::message')
			@include('admin.template.partials.erros')
            </div>
            @yield('content')
        </div>
        @yield('js')
        <script>
            $('div.alert').not('.alert-important').delay(3000).fadeOut(350);
        </script>
    </body>
</html>
