@extends('admin.template.main')
@section('title','Listado de Categorias')

@section('section')
	<h3>Listado de categorias</h3>
@endsection

@section('content')
	<a href="{{route('admin.categories.create')}}"><button class="btn btn-info">Registrar Categoria</button></a>
	<table class="table table-striped table-bordered">
	<thead>
		<th>ID</th>
		<th>Nombre</th>
		<th>Acción</th>
	</thead>
	<tbody>
		@foreach($categories as $category)
		<tr>
			<td>{{ $category->id }}</td>
			<td>{{ $category->name }}</td>
			<td>
					<a href="{{route('admin.categories.edit',$category->id)}}" class="btn btn-warning"><span class="glyphicon glyphicon-wrench" aria-hidden="True"></span></a>
					<a href="{{route('admin.categories.destroy',$category->id)}}" class="btn btn-danger" onclick="return confirm('¿Deseas Eliminar el usuario?')"><span class="glyphicon glyphicon-remove-circle" aria-hidden="True"></span></a>
			</td>
		</tr>
		@endforeach
	</tbody>
	</table>
	<div class="text-center">
		{!! $categories->render() !!}
	</div>
@endsection()