@extends('admin.template.main')

@section('content')
    <h3 class="title-front left">{{trans('app.title')}}</h3>

    @if(Auth::user()) 
      <h2>{{ trans('app.welcome',['name'=>Auth::user()->name]) }}</h2>
    @endif
    <div class="row">
      <div class="col-md-8">
      <div class="row">
        @foreach($articles as $article)
          <div class="col-md-6">
          <div class="panel panel-default">
          <div class="panel-body ">
            <a href="{{ route('articles.view',$article->slug) }}" class="thumbnail ">
            @foreach($article->images as $image)
              <img class="img-responsive img-article homeart" src="/images/articles/{{$image->name}}">
            @endforeach
                <hr>
                <h3>{{$article->title}}</h3>
                </a>
                <div class="pull-left">
                  <a href="{{route('search.category',$article->category->name)}}">{{$article->category->name}}</a>
                    
                </div>
                <div class="pull-right">
                    {{$article->created_at->diffForHumans()}}
                </div>
           </div>
           </div>     
           </div>
        @endforeach()
    {!! $articles->render() !!}
      </div>   
      </div>
      <div class="col-md-4 aside">
        @include('front.aside')
      </div>
    </div>
@endsection
    


