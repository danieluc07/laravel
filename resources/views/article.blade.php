@extends('admin.template.main')

@section('content')

    <h3 class="title-front left">{{$article->title}}</h3>
    <hr>
    <div class="row">
    	<div class="col-md-8">
    		<img class="img-responsive img-article imgsor" src="/images/articles/{{$image->name}}">
    		<p class="identp">
                {!! $article->content !!}
            </p>
    		<hr>
    		<div class="pull-left">
    			<a href="{{route('search.category',$article->category->name)}}">{{$article->category->name}}</a>
    		</div>
    		<div class="pull-right">
    			{{$article->created_at->diffForHumans()}}
    		</div>
        </div>
        <div class="col-md-4 aside">
            @include('front.aside')
        </div>
    </div
@endsection