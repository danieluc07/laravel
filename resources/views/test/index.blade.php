<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

<title>{{$article->title}}</title>

<meta name="description" content="The HTML5 Herald">
<meta name="author" content="SitePoint">
<link rel="stylesheet" href="{{asset('css/pp.css')}}">

</head>
<body>
  <script src="js/scripts.js"></script>
 {{$article->content}}
 <hr>
{{$article->user->name}} // {{$article->category->name}}<br>
@foreach ($article->tags as $tag)
	{{$tag->name}}
@endforeach

</body>
</html>

