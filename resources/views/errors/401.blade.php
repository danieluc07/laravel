@extends('admin.template.main')

@section('content')
	<h3>No tienes permiso para entrar, no eres administrador.</h3>
	<img src="/images/denied.jpg">
@endsection
