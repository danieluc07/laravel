<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model 
{
  

    protected $table = "articles";
    protected $fillable = ['name','content','category_id','user_id','slug'];
    
    public function category(){
    	return $this->belongsTo('App\Category');
    }

    public function user(){
    	return $this->belongsTo('App\User');
    }
    public function images(){
    	return $this->hasMany('App\Image');
    }

    public function tags(){
    	return $this->belongsToMany('App\Tag');
    }

    public function scopeSearch($query,$name){
        return $query->where('title','LIKE',"%$name%");
    }
    public function scopeSearchArticle($query,$name){
        return $query->where('slug','=',$name);
    }
}
