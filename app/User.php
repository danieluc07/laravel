<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    
    protected $table = "users";

    protected $fillable = ['name', 'email', 'password','type'];

    
    protected $hidden = ['password', 'remember_token'];

    public function articles(){
        return $this->hasMany('App\Article');
    
    }

    public function isAdmin(){
    	return $this->type==='admin';
    }
}
