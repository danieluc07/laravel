<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', [
	'uses'=>'FrontController@index',
	'as'=>'/'
	]);

Route::get('categories/{name}',[
	'uses'=>'FrontController@searchCategory',
	'as'  => 'search.category'
	]);

Route::get('tags/{name}',[
	'uses'=>'FrontController@searchTags',
	'as'  => 'search.tags'
	]);
Route::get('articles/{slug}',[
		'uses' => 'FrontController@viewArticle',
		'as'   => 'articles.view'
	]);

Route::group(['prefix'=>'admin','middleware'=>['auth']],function(){
	
	Route::group(['middleware'=>'admin'],function(){

		Route::resource('users','UsersController');
		
		Route::get('users/{id}/destroy',[
		'uses'=>'UsersController@destroy',
		'as'=>'admin.users.destroy'
		]);

	});
	
	Route::resource('categories','CategoriesController');
	Route::resource('tags','TagsController');
	Route::resource('articles','ArticlesController');

	

	Route::get('categories/{id}/destroy',[
		'uses'=>'CategoriesController@destroy',
		'as'=>'admin.categories.destroy'
		]);

	Route::get('tags/{id}/destroy',[
		'uses'=>'tagsController@destroy',
		'as'=>'admin.tags.destroy'
		]);

	Route::get('articles/{id}/destroy',[
		'uses'=>'ArticlesController@destroy',
		'as'=>'admin.articles.destroy'
		]);

	Route::get('images',[
		'uses'=>'ImageController@index',
		'as' => 'admin.images.index'
		]);
	
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('admin/auth/login',[
	'uses'=>'Auth\AuthController@getLogin',
	'as'=>'admin.auth.login'
	]);

Route::post('admin/auth/login',[
	'uses'=>'Auth\AuthController@postLogin',
	'as'=>'admin.auth.login'
	]);

Route::get('admin/auth/logout',[
	'uses'=>'Auth\AuthController@logout',
	'as'=>'admin.auth.logout'
	]);

