<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Requests\CategoryRequest;
use App\Tag;
use Laracasts\Flash\Flash;

class TagsController extends Controller
{

    public function index(Request $request)
    {
        $tags=Tag::search($request->name)->orderBy('id','DESC')->paginate(5);
        return view ('admin.tags.index')->with('tags',$tags); 
    }

    public function create()
    {
        return view('admin.tags.create');
    }


    public function store(CategoryRequest $request)
    {
        $tag = new Tag($request->All());
        $tag->save();
        Flash::Message("Etiqueta Registrada");
        return redirect()->route('admin.tags.index');
    }

    public function show($id)
    {
       
    }


    public function edit($id)
    {
        $tag=Tag::find($id);
        return view ('admin.tags.edit')->with('tags',$tag);
    }


    public function update(CategoryRequest $request, $id)
    {
        $tag=Tag::find($id);
        $tag->fill($request->all());
        $tag->save();
        flash('La etiqueta: '.$tag->name.', fue editada', 'success');
        return redirect()->route('admin.tags.index');
    }


    public function destroy($id)
    {
        $tag=Tag::find($id);
        $tag->delete();
        flash('La etiqueta: '.$tag->name.', fue eliminado', 'danger');
        return redirect()->route('admin.tags.index');
    }
}
