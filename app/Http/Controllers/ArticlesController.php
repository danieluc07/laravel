<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Http\Requests\ArticleRequest;
use App\Image;
use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Laracasts\Flash\Flash;

class ArticlesController extends Controller
{
    public function index(Request $request)
    {
        $articles = Article::search($request->title)->orderBy('id', 'DESC')->paginate(5);
        $articles->each(function ($articles) {
            $articles->category;
            $articles->user;

        });
        return view('admin.articles.index')->with('articles', $articles);
    }

    public function create()
    {
        $categories = Category::orderBy('name', 'ASC')->lists('name', 'id');
        $tags       = Tag::orderBy('name', 'ASC')->lists('name', 'id');

        return view('admin.articles.create')->with('categories', $categories)->with('tags', $tags);
    }

    public function store(ArticleRequest $request)
    {

        $article          = new Article($request->all());
        $article->user_id = \Auth::user()->id;
        $article->title   = $request->title;
        $article->slug    = str_slug($request->title);
        $article->save();

        $article->tags()->sync($request->tags);

        $image = new Image();
        $image->article()->associate($article);

        if ($request->file('image')) {
            $file = $request->file('image');
            $name = 'blog_' . time() . '.' . $file->getClientOriginalExtension();
            $path = public_path() . '/images/articles';
            $file->move($path, $name);
            $image->name = $name;
        }

        $image->save();

        flash('El articulo fue agregado', 'success');
        return redirect()->route('admin.articles.index');
    }

    public function show($id)
    {

    }

    public function edit($id)
    {
        $article    = Article::find($id);
        $categories = Category::orderBy('name', 'DESC')->lists('name', 'id');
        $tags       = Tag::orderBy('name', 'DESC')->lists('name', 'id');
        return view('admin.articles.edit')->with('article', $article)->with('categories', $categories)->with('tags', $tags);
    }

    public function update(Request $request, $id)
    {

        $art = Article::find($id);
        $art->fill($request->all());
        $art->title = $request->title;
        $art->slug  = str_slug($request->title);
        $art->save();
        $art->tags()->sync($request->tags);
        flash('El articulo: ' . $art->title . ', fue editado.', 'success');
        return redirect()->route('admin.articles.index');
    }

    public function destroy($id)
    {
        $cat = Article::find($id);
        $cat->delete();
        flash('El articulo: ' . $cat->title . ', fue eliminado', 'danger');
        return redirect()->route('admin.articles.index');
    }
}
