<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Article;
use App\Category;
use App\Tag;
use App\Image;
use Carbon\Carbon;

class FrontController extends Controller
{
	public function __construct(){
		Carbon::setlocale('es');
	}
    public function index(){
    	$articles= Article::orderBy('id','DESC')->paginate(10);

    	$articles->each(function($articles){
    		$articles->category;
    		$articles->images;
    	});
    	return view('home')->with('articles',$articles);
    }

    public function searchCategory($name){
        $category=Category::SearchCategory($name)->first();
        $articles=$category->articles()->paginate(4);
        $articles->each(function($articles){
            $articles->category;
            $articles->images;
        });
        return view('home')->with('articles',$articles);
    }

    public function searchTags($name){
        $tags=Tag::SearchTag($name)->first();
        $articles = $tags->articles()->paginate(4);
        $articles->each(function($articles){
            $articles->category;
            $articles->images;
        });
        return view('home')->with('articles',$articles);

    }

    public function viewArticle($slug){
        $article=Article::SearchArticle($slug)->first();
        $article->category;
        $article->user;
        $article->tags;
        $image=Image::find($article->id);
        return view('article')->with('article',$article)->with('image',$image);
    }
}
