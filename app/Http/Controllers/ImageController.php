<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ImageController extends Controller
{
    public function index()
    {
        $images= \App\Image::all();
        return view('admin.images.index')->with('images',$images);
    }    
}
